# System Threading Tasks Extensions

## What

	-System.Threading.Tasks.Extensions version 4.5.3
	-Provides additional types that simplify the work of writing concurrent and asynchronous code.
	-Commonly Used Types:
		+ System.Threading.Tasks.ValueTask<TResult>

## Requirements
[![Unity 2018.3+](https://img.shields.io/badge/unity-2018.3+-brightgreen.svg?style=flat&logo=unity&cacheSeconds=2592000)](https://unity3d.com/get-unity/download/archive)
[![.NET 2.0 Scripting Runtime](https://img.shields.io/badge/.NET-2.0-blueviolet.svg?style=flat&cacheSeconds=2592000)](https://docs.unity3d.com/2018.3/Documentation/Manual/ScriptingRuntimeUpgrade.html)


## Installation

```bash
"com.yenmoc.system-threading-tasks-extensions":"https://gitlab.com/yenmoc/system-threading-tasks-extensions"
or
npm publish --registry http://localhost:4873
```

